import Logo from "components/assets/Logo";
import RegisterBtn from "components/assets/RegisterBtn";
import Navbar from "./Navbar";
import { HeaderTag } from "./view";

const Header = () => (
  <HeaderTag className="d-flex align-items-center justify-content-around">
    <Logo />
    <Navbar />
    <RegisterBtn />
  </HeaderTag>
);

export default Header;
