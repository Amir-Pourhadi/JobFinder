import { NavLink } from "react-router-dom";
import styled from "styled-components";

export const Link = styled(NavLink).attrs({ activeClassName: "active" })(({ theme: { colors } }) => ({
  fontFamily: "Helvetica",
  fontSize: 20,
  fontWeight: "bold",
  color: "black",
  "&:hover, &.active": {
    color: colors.orange,
    fontStyle: "italic",
  },
}));
