import { bool, string } from "prop-types";
import { Link } from "./view";

const NavLink = ({ link: { name, path, exact } }) => (
  <Link to={path} exact={exact}>
    {name}
  </Link>
);

NavLink.propTypes = { name: string, path: string, exact: bool };
NavLink.defaultProps = { name: "NavLink", path: "/here", exact: false };

export default NavLink;
