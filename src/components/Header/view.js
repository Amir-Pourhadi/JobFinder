import styled from "styled-components";

export const HeaderTag = styled.header({
  height: 75,
  boxShadow: "0 5px 10px -2px grey",
});
