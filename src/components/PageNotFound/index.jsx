import PrimaryBtn from "components/assets/PrimaryBtn";
import { Helmet } from "react-helmet";
import { Gif, Main } from "./view";

const button = { content: "Go Back to Home", btnDesc: "", link: "/" };

const PageNotFound = () => (
  <>
    <Helmet>
      <link rel="preconnect" href="https://fonts.googleapis.com" />
      <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin />
      <link rel="stylesheet" href="https://fonts.googleapis.com/css2?family=Arvo&display=swap" />
    </Helmet>
    <Main>
      <Gif/>
      <div className="d-flex flex-column align-items-center">
        <h3>Looks like you're lost 😢</h3>
        <p>The page you are looking for, is not available!</p>
        <PrimaryBtn button={button} />
      </div>
    </Main>
  </>
);

export default PageNotFound;
