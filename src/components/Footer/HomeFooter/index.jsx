import { FaGithub, FaInstagram, FaLinkedinIn, FaTelegramPlane, FaTwitter } from "react-icons/fa";
import { SiGmail } from "react-icons/si";
import { Counter, CounterText, Icon, Main } from "./view";

const HomeFooter = () => (
  <Main className="row m-0" height={100}>
    <div className="col-7">
      <div className="d-flex justify-content-center">Stats</div>
      <div className="d-flex flex-column align-items-center">
        <div className="d-flex">
          <Counter id="user-count" className="me-3">
            1,500,658
          </Counter>
          <CounterText className="text-capitalize">registered users</CounterText>
        </div>
        <div className="d-flex">
          <Counter id="job-count" className="me-3">
            1,500,658
          </Counter>
          <CounterText className="text-capitalize">total jobs posted</CounterText>
        </div>
      </div>
    </div>
    <div className="col-5">
      <div className="d-flex justify-content-center">Social Media</div>
      <ul className="list-unstyled d-flex flex-wrap justify-content-around my-2 m-0">
        <li>
          <Icon color="#0274B3" href="https://linkedin.com/in/amirpourhadi">
            <FaLinkedinIn />
          </Icon>
        </li>
        <li>
          <Icon color="#f09433" href="https://instagram.com/amir.pourhadi.official">
            <FaInstagram />
          </Icon>
        </li>
        <li>
          <Icon color="#1C8FBE" href="https://t.me/AmirPourhadiOfficial">
            <FaTelegramPlane />
          </Icon>
        </li>
        <li>
          <Icon color="#E53F33" href="mailto:Alex.CE1379@Gmail.com">
            <SiGmail />
          </Icon>
        </li>
        <li>
          <Icon color="#1A1D21" href="https://github.com/Amir-Pourhadi">
            <FaGithub />
          </Icon>
        </li>
        <li>
          <Icon color="#58A0D2" href="https://twitter.com/AmirPourhadi">
            <FaTwitter />
          </Icon>
        </li>
      </ul>
    </div>
  </Main>
);

export default HomeFooter;
