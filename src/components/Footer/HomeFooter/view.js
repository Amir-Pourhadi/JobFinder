import styled from "styled-components";

export const Main = styled.div(({ theme: { colors }, height }) => ({
  minHeight: height,
  backgroundColor: colors.footer,
  color: "white",
  fontSize: 25,
}));

export const Counter = styled.div({
  fontSize: 20,
});

export const CounterText = styled.div({
  fontSize: 20,
  color: "#C3C3C3",
});

export const Icon = styled.a(({ color }) => ({
  "&:hover": { color },
}));
