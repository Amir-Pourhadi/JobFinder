import { useLocation } from "react-router-dom";
import CommonFooter from "./CommonFooter";
import HomeFooter from "./HomeFooter";

const Footer = () => {
  const { pathname } = useLocation();

  return (
    <footer>
      {pathname === "/" && <HomeFooter />}
      <CommonFooter />
    </footer>
  );
};

export default Footer;
