import { FaGitlab } from "react-icons/fa";
import styled from "styled-components";

export const Detail = styled.div(({ theme: { colors }, height }) => ({
  minHeight: height,
  backgroundColor: colors.footer,
}));

export const Text = styled.p({
  display: "flex",
  alignItems: "center",
  fontSize: 20,
  color: "white",
  margin: 0,
});

export const Link = styled.a(({ theme: { colors } }) => ({
  "&:hover": {
    color: colors.orange,
  },
}));

export const GitLabIcon = styled(FaGitlab)({
  verticalAlign: "top",
  fontSize: 28,
  marginLeft: 12,
  color: "#EF9B24",
});
