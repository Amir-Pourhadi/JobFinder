import { Detail, GitLabIcon, Link, Text } from "./view";

const CommonFooter = () => (
  <Detail className="d-flex justify-content-around align-items-center" height={40}>
    <Text>&copy; Copyright 2021, JobFinder Inc. Amir Pourhadi, All Rights Reserved.</Text>
    <Text>
      View Source Code on &nbsp;
      <Link href="https://gitlab.com/Amir-Pourhadi/JobFinder" target="/">
        GitLab
        <GitLabIcon />
      </Link>
    </Text>
  </Detail>
);

export default CommonFooter;
