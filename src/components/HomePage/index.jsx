import { pages } from "data/db.json";
import { Helmet } from "react-helmet";
import Section from "./Section";
import { Main } from "./veiw";

const { sections } = pages.find(({ name }) => name === "Home");

const HomePage = () => (
  <>
    <Helmet>
      <title>Welcome to Job Finder</title>
    </Helmet>
    <Main>
      {sections.map((section, index) => (
        <Section data={section} key={index} />
      ))}
    </Main>
  </>
);

export default HomePage;
