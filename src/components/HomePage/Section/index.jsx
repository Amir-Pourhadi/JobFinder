import PrimaryBtn from "components/assets/PrimaryBtn";
import aboutUs from "images/about-us.jpg";
import employer from "images/employer.jpg";
import jobSeeker from "images/job-seeker.jpg";
import { array, object, shape, string } from "prop-types";
import { Desc, Image, SectionTag, Title } from "./view";

const images = { jobSeeker, employer, aboutUs };

const Section = ({ data: { name, title, desc, button, height, background } }) => (
  <SectionTag className="row p-5 m-0" height={height} background={background}>
    <div className="col-6 d-flex align-items-center flex-column justify-content-around">
      <Title className="text-center">{title}</Title>
      <Desc>{desc}</Desc>
      <PrimaryBtn button={button} />
    </div>
    <div className="col-6 d-flex align-items-center">
      <Image src={images[name]} alt={`${name} Image`} />
    </div>
  </SectionTag>
);

Section.prototypes = {
  data: shape({
    name: string,
    title: string,
    desc: string,
    button: object,
    height: string,
    background: array,
  }),
};

export default Section;
