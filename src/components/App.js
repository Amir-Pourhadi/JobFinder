import "bootstrap/dist/css/bootstrap.min.css";
import { Route, Switch } from "react-router-dom";
import { ThemeProvider } from "styled-components";
import Footer from "./Footer";
import { GlobalStyle } from "./GlobalStyle";
import Header from "./Header";
import HomePage from "./HomePage";
import PageNotFound from "./PageNotFound";
import Theme from "./Theme";

export default function App() {
  return (
    <ThemeProvider theme={Theme}>
      <div className="container-fluid p-0">
        <GlobalStyle />
        <Header />
        <Switch>
          <Route exact path="/" component={HomePage} />
          <Route component={PageNotFound} />
        </Switch>
        <Footer />
      </div>
    </ThemeProvider>
  );
}
