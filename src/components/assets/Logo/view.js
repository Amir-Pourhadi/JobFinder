import { Link } from "react-router-dom";
import styled from "styled-components";

export const Div = styled(Link)({
  color: "black",
  "&:hover": {
    color: "black",
  },
});

export const Img = styled.img({ width: 43 });

export const Text = styled.span({
  fontFamily: "script mt, sans-serif",
  fontSize: 36,
});

export const TextOrange = styled.span(({ theme: { colors } }) => ({
  color: colors.orange,
}));
