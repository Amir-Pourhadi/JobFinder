import logoImage from "./logo-img.png";
import { Div, Img, Text, TextOrange } from "./view";

const Logo = () => (
  <Div to="/">
    <Img src={logoImage} alt="logo-img" />
    <Text>
      ob <TextOrange>Finder</TextOrange>
    </Text>
  </Div>
);

export default Logo;
