import { exact, string } from "prop-types";
import { Link } from "react-router-dom";
import { AngleRight, BtnDesc, Text } from "./view";

const PrimaryBtn = ({ button: { content, btnDesc, link } }) => (
  <div>
    <Link className="btn btn-primary d-flex align-items-end" to={link}>
      <Text>{content}</Text>
      <AngleRight />
    </Link>
    <BtnDesc className="mt-2">{btnDesc}</BtnDesc>
  </div>
);

PrimaryBtn.propTypes = { button: exact({ content: string, btnDesc: string, link: string }) };

export default PrimaryBtn;
