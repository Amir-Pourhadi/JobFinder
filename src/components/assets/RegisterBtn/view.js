import { NavLink } from "react-router-dom";
import styled from "styled-components";

export const Button = styled(NavLink)({ width: 140 });
export const Text = styled.span({ fontSize: 20 });
