# Job Finder

Find your suitable job as easy as ABC!

## Links

<a href="https://gitLab.com/Amir-Pourhadi/JobFinder">
<img src="https://img2.pngio.com/filegitlab-logosvg-wikimedia-commons-gitlab-png-1200_1109.png" width="30">&nbsp;&nbsp;Repository</a>

<br>

<a href="https://miro.com/app/board/o9J_l0w-Yfo=">
<img src="https://miro.com/blog/wp-content/uploads/2020/10/logo-large-1.svg" width="30">&nbsp;&nbsp;Mind Map</a>

<br>

<a href="https://trello.com/b/jmvEobOE">
<img src="https://seeklogo.com/images/T/trello-logo-CE7B690E34-seeklogo.com.png" width="30">&nbsp;&nbsp;Trello</a>

<br>

<a href="https://xd.adobe.com/view/a978a854-cdb7-4bf0-8199-8b01e2bcd450-fc2b/?fullscreen">
<img src="https://upload.wikimedia.org/wikipedia/commons/thumb/c/c2/Adobe_XD_CC_icon.svg/1200px-Adobe_XD_CC_icon.svg.png" width="30">&nbsp;&nbsp;Design</a>

<br>

<a href="https://amir-job-finder.herokuapp.com/">
<img src="https://cdn.icon-icons.com/icons2/2108/PNG/512/heroku_icon_130912.png" width="30">&nbsp;&nbsp;Deploy</a>

<br>
<br>

## How to start the project

This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).

### `npm start`

Runs the app in the development mode.\
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

The page will reload if you make edits.\
You will also see any lint errors in the console.
